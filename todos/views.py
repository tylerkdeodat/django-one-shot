from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy


# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"

class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    success_url = reverse_lazy("todolist_list")

class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name", ]

    def get_success_url(self):
        return reverse_lazy("todolist_detail", args=[self.object.id])

class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todolist_list")

    # def form_valid(self, form):
    #     form.instance.author = self.request.user
    #     return super().form_valid(form)


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "item/new.html" 
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todolist_list")

class TodoItemUpdateView(UpdateView):
    model = TodoItem
    templet_name = "item/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todolist_list")
    