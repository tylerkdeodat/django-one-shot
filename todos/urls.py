from django.urls import path

from todos.views import (
    TodoListListView,
    TodoListDetailView,
    TodoListCreateView,
    TodoListUpdateView,
    TodoListDeleteView,
    TodoItemCreateView,
    TodoItemUpdateView,
)


urlpatterns = [
    path("", TodoListListView.as_view(), name="todolist_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todolist_detail"),
    path("create/", TodoListCreateView.as_view(), name="todolist_new"),
    path("<int:pk>/edit", TodoListUpdateView.as_view(), name="todolist_edit"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todolist_delete"),
    path("items/create/", TodoItemCreateView.as_view(), name="todoitem_create"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="todoitem_edit")
]